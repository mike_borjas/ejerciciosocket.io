const path = require('path')
const express = require('express');
const app = express();

//Settings
app.set('port',3000);

//Static files

app.use(express.static(path.join(__dirname, "public")))

const server= app.listen(app.get('port'),() =>{
    console.log('server on port', app.get('port'));
})


//Websockets
const SocketIO = require('socket.io');
const io = SocketIO.listen(server);

io.on('connection', (socket) =>{
    console.log("New Connection", socket.id);

    socket.on('chat:message', (data) =>{
        io.sockets.emit('chat:message', data);

    });

    socket.on('chat:typing',(data) =>{
        socket.broadcast.emit('chat:typing', data);

    });
})


